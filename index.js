const express = require("express");
const cors = require("cors");
const jwt = require("jsonwebtoken");

require("dotenv").config();

const { MongoClient, ServerApiVersion, ObjectId } = require("mongodb");
const app = express();
const port = process.env.PORT || 5000;

// middle ware
app.use(cors());
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// main app

const uri = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0.eyven.mongodb.net/?retryWrites=true&w=majority`;
console.log(uri);
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  serverApi: ServerApiVersion.v1,
});
function verifyJWT(req, res, next) {
  console.log("jwt");
  const authHeader = req.headers.authorization;
  if (!authHeader) {
    return res.status(401).send({ message: "UnAuthorized access" });
  }
  const token = authHeader.split(" ")[1];
  jwt.verify(token, process.env.ACCESS_TOKEN, function (err, decoded) {
    if (err) {
      return res.status(403).send({ message: "forbidden access" });
    }
    req.decoded = decoded;
    next();
  });
}
async function run() {
  try {
    await client.connect();
    const partCollections = client
      .db("car_manufracturar")
      .collection("car_parts");
    const ordersCollection = client
      .db("car_manufracturar")
      .collection("orders");
    const reviewCollection = client
      .db("car_manufracturar")
      .collection("reviews");
    const profileCollection = client
      .db("car_manufracturar")
      .collection("profiles");
    const userCollection = client.db("car_manufracturar").collection("users");
    // get all data
    app.get("/get-parts", async (req, res) => {
      const query = {};
      const result = await partCollections.find(query).toArray();
      res.send(result);
    });

    // get single data
    app.get("/get-parts/:id", async (req, res) => {
      const { id } = req.params;
      const filter = { _id: ObjectId(id) };
      const result = await partCollections.findOne(filter);
      res.send(result);
    });

    // add data
    app.post("/add-parts", async (req, res) => {
      const data = req.body;
      const result = await partCollections.insertOne(data);
      res.send(result);
    });
    app.delete("/delete_parts/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: ObjectId(id) };
      const result = await partCollections.deleteOne(query);
      res.send(result);
    });
    app.put("/update-part/:id", async (req, res) => {
      const { id } = req.params;
      const data = req.body;
      const filter = { _id: ObjectId(id) };
      const options = { upsert: true };
      const updateDoc = {
        $set: {
          data,
        },
      };
      const result = await partCollections.updateOne(
        filter,
        updateDoc,
        options
      );
      res.send(result);
    });

    // my orders using
    app.post("/post-orders", async (req, res) => {
      const data = req.body;
      const result = await ordersCollection.insertOne(data);
      res.send(result);
    });

    // delete method
    app.delete("/order_delete/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: ObjectId(id) };
      const result = await ordersCollection.deleteOne(query);
      res.send(result);
    });
    app.get("/get-orders", verifyJWT, async (req, res) => {
      const email = req.query.email;
      const query = { email: email };
      const decodEmail = req.decoded.email;
      if (email === decodEmail) {
        const result = await ordersCollection.find(query).toArray();
        return res.send(result);
      } else {
        return res.status(403).send({ message: "forbidden access" });
      }
    });
    app.get("/total_orders", verifyJWT, async (req, res) => {
      const query = {};
      const result = await ordersCollection.find(query).toArray();
      return res.send(result);
    });
    // POST REVIEWS
    app.post("/add-review", async (req, res) => {
      const data = req.body;
      const result = await reviewCollection.insertOne(data);
      res.send(result);
    });
    app.get("/add-review", async (req, res) => {
      const query = {};
      const result = await reviewCollection.find(query).toArray();
      res.send(result);
    });
    // MY PROFILE UPDATE
    app.put("/profile", async (req, res) => {
      const { email } = req.query.email;
      const data = req.body;
      const filter = { email: email };
      const options = { upsert: true };
      const updateDoc = {
        $set: {
          data,
        },
      };
      const result = await profileCollection.updateOne(
        filter,
        updateDoc,
        options
      );
      res.send(result);
    });

    // users
    app.get("/users", verifyJWT, async (req, res) => {
      const usres = await userCollection.find().toArray();
      res.send(usres);
    });
    app.get("/admin/:email", async (req, res) => {
      const email = req.params.email;
      const user = await userCollection.findOne({ email: email });
      isAdmin = user.role === "admin";
      res.send({ admin: isAdmin });
    });
    // make admin
    app.put("/users/admin/:email", verifyJWT, async (req, res) => {
      const email = req.params.email;
      console.log(email);
      const reqEmail = req.decoded.email;
      const reqEmailOwnerAccount = await userCollection.findOne({
        email: reqEmail,
      });
      if (reqEmailOwnerAccount.role === "admin") {
        const filter = { email: email };
        const updateDoc = {
          $set: { role: "admin" },
        };
        const result = await userCollection.updateOne(filter, updateDoc);
        res.send(result);
      } else {
        res.status(403).send({ message: "forbidden access" });
      }
    });
    // PUT
    app.put("/users/:email", async (req, res) => {
      const email = req.params.email;
      const user = req.body;
      const filter = { email: email };
      const options = { upsert: true };

      const updateDoc = {
        $set: user,
      };
      const token = jwt.sign({ email: email }, process.env.ACCESS_TOKEN, {
        expiresIn: "1h",
      });
      const result = await userCollection.updateOne(filter, updateDoc, options);
      res.send({ result, token });
    });
  } finally {
  }
}
run().catch(console.dir);

app.get("/", (req, res) => {
  res.send("working car server ");
});

app.get("/test", (req, res) => {
  res.send("test is working");
});
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
